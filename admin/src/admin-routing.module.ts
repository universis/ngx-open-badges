import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { BadgeClassesHomeComponent } from './components/badge-classes-home/badge-classes-home.component';
import { BadgeClassesTableComponent } from './components/badge-classes-table/badge-classes-table.component';
import { BadgeClassesRootComponent } from './components/badge-classes-root/badge-classes-root.component';
import { BadgeClassesDashboardComponent } from './components/badge-classes-dashboard/badge-classes-dashboard.component';
import { BadgeClassesOverviewComponent } from './components/badge-classes-dashboard/badge-classes-dashboard-overview/badge-classes-overview.component';
import { BadgeImagesHomeComponent } from './components/badge-images-home/badge-images-home.component';
import { BadgeImagesTableComponent } from './components/badge-images-table/badge-images-table.component';
import { BadgeImagesRootComponent } from './components/badge-images-root/badge-images-root.component';
import { BadgeProfilesHomeComponent } from './components/badge-profiles-home/badge-profiles-home.component';
import { BadgeProfilesTableComponent } from './components/badge-profiles-table/badge-profiles-table.component';
import { BadgeProfilesRootComponent } from './components/badge-profiles-root/badge-profiles-root.component';
import { GenericTableConfigurationResolver, GenericTableSearchResolver, StudentDefaultTableConfigurationResolver } from './components/generic/generic-table-config.resolver';
import { BadgeClassesDashboardStudentsComponent } from './components/badge-classes-dashboard/badge-classes-dashborad-students/badge-classes-dashboard-students.component';
import { BadgeClassesAddStudentComponent } from './components/badge-classes-dashboard/badge-classes-dashborad-students/badge-classes-add-student.component';
import { BadgeAssertionsViewComponent } from './components/badge-assertions-view/badge-assertions-view.component';
import { AdvancedFormRouterComponent } from "@universis/forms";
import { AdvancedFormItemResolver, AdvancedFormModalData, AdvancedFormParentItemResolver } from '@universis/forms';

import { ItemRulesModalComponent, RuleFormModalData } from '@universis/ngx-rules'

/*
badges/{classes,images,profiles}/list/:list
badges/{classes,images,profiles}/:id/:action
badges/{classes,images,profiles}/create/:action
badges/classes/:id/dashboard/overview
badges/classes/:id/dashboard/students
badges/classes/:id/dashboard/students/(modal:add)
badges/alignments/
*/

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'classes'
  },
  {
    path: '',
    children: [

      {
        path: 'classes',
        children: [
          {
            path: '',
            component: BadgeClassesHomeComponent,
            data: {
              model: 'OpenBadgeClasses', config: 'badge-classes'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list/index'
              },
              {
                path: 'list',
                pathMatch: 'full',
                redirectTo: 'list/index'
              },
              {
                path: 'list/:list',
                component: BadgeClassesTableComponent,
                data: {
                  model: 'OpenBadgeClasses', config: 'badge-classes'
                },
                resolve: {
                  tableConfiguration: GenericTableConfigurationResolver,
                  searchConfiguration: GenericTableSearchResolver
                }
              }
            ]
          },
          {
            path: 'create',
            component: BadgeClassesRootComponent,
            data: {
              model: 'OpenBadgeClasses', config: 'badge-classes'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'new'
              },
              {
                path: ':action',
                component: AdvancedFormRouterComponent,
                data: {
                  model: 'OpenBadgeClasses',
                  serviceParams: {
                    $expand: 'tags, criteria'
                  }
                }
              }
            ]
          },
          {
            path: ':id',
            component: BadgeClassesRootComponent,
            data: {
              model: 'OpenBadgeClasses', config: 'badge-classes'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'edit'
              },
              {
                path: 'dashboard',
                component: BadgeClassesDashboardComponent,
                data: {
                  title: 'OpenBadges.BadgeClasses.Overview'
                },
                children: [
                  {
                    path: '',
                    redirectTo: 'overview',
                    pathMatch: 'full'
                  },
                  {
                    path: 'overview',
                    component: BadgeClassesOverviewComponent,
                    data: {
                      title: 'OpenBadges.BadgeClasses.Overview'
                    },
                    children: [
                      {
                        path: 'credentials/:id/rules',
                        pathMatch: 'full',
                        component: ItemRulesModalComponent,
                        outlet: 'modal',
                        data: <RuleFormModalData>{
                          model: 'EducationalOccupationalCredentials',
                          closeOnSubmit: true,
                          navigationProperty: 'Rules'
                        },
                        resolve: {
                          data: AdvancedFormItemResolver
                        }
                      },
                    ]
                  },
                  {
                    path: 'students',
                    component: BadgeClassesDashboardStudentsComponent,
                    data: {
                      title: 'OpenBadges.BadgeClasses.Students',
                      model: "OpenBadgeClasses", config: "badge-classes-dashboard-students"
                    },
                    resolve: {
                      tableConfiguration: GenericTableConfigurationResolver,
                    },
                    children: [
                      {
                        path: 'add',
                        pathMatch: 'full',
                        component: BadgeClassesAddStudentComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                          title: 'OpenBadges.AddStudent'
                        },
                        resolve: {
                          badgeClass: AdvancedFormParentItemResolver,
                          config: StudentDefaultTableConfigurationResolver,
                        }
                      }
                    ]
                  }
                ]
              },
              {
                path: ':action',
                component: AdvancedFormRouterComponent,
                data: {
                  model: 'OpenBadgeClasses',
                  serviceParams: {
                    $expand: 'tags, criteria'
                  }
                }
              }
            ]
          }
        ]
      },

      {
        path: 'images',
        children: [
          {
            path: '',
            component: BadgeImagesHomeComponent,
            data: {
              model: 'BadgeImages', config: 'badge-images'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list/index'
              },
              {
                path: 'list',
                pathMatch: 'full',
                redirectTo: 'list/index'
              },
              {
                path: 'list/:list',
                component: BadgeImagesTableComponent,
                data: {
                  model: 'BadgeImages', config: 'badge-images'
                },
                resolve: {
                  tableConfiguration: GenericTableConfigurationResolver,
                  searchConfiguration: GenericTableSearchResolver
                }
              }
            ]
          },
          {
            path: 'create',
            component: BadgeImagesRootComponent,
            data: {
              model: 'BadgeImages', config: 'badge-images'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'new'
              },
              {
                path: ':action',
                component: AdvancedFormRouterComponent,
                data: {
                  model: 'OpenBadgeImageObjects',
                  serviceParams: {
                    $expand: ''
                  }
                }
              }
            ]
          },
          {
            path: ':id',
            component: BadgeImagesRootComponent,
            data: {
              model: 'BadgeImages', config: 'badge-images'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'edit'
              },
              {
                path: ':action',
                component: AdvancedFormRouterComponent,
                data: {
                  model: 'OpenBadgeImageObjects',
                  serviceParams: {
                    $expand: ''
                  }
                }
              }
            ]
          }
        ]
      },

      {
        path: 'profiles',
        children: [
          {
            path: '',
            component: BadgeProfilesHomeComponent,
            data: {
              model: 'BadgeProfiles', config: 'badge-profiles'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list/index'
              },
              {
                path: 'list',
                pathMatch: 'full',
                redirectTo: 'list/index'
              },
              {
                path: 'list/:list',
                component: BadgeProfilesTableComponent,
                data: {
                  model: 'BadgeProfiles', config: 'badge-profiles'
                },
                resolve: {
                  tableConfiguration: GenericTableConfigurationResolver,
                  searchConfiguration: GenericTableSearchResolver
                }
              }
            ]
          },
          {
            path: 'create',
            component: BadgeProfilesRootComponent,
            data: {
              model: 'BadgeProfiles', config: 'badge-profiles'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'new'
              },
              {
                path: ':action',
                component: AdvancedFormRouterComponent,
                data: {
                  model: 'OpenBadgeProfiles',
                  serviceParams: {
                    $expand: ''
                  }
                }
              }
            ]
          },
          {
            path: ':id',
            component: BadgeProfilesRootComponent,
            data: {
              model: 'BadgeProfiles', config: 'badge-profiles'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'edit'
              },
              {
                path: ':action',
                component: AdvancedFormRouterComponent,
                data: {
                  model: 'OpenBadgeProfiles',
                  serviceParams: {
                    $expand: ''
                  }
                }
              }
            ]
          }
        ]
      },

      // TODO create alignments

      {
        path: 'assertions/:id',
        component: BadgeAssertionsViewComponent
      }
    ]
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class OpenBadgesRoutingModule { }
