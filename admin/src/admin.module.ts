import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdvancedFormsModule } from '@universis/forms';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SharedModule } from '@universis/common';
import { TablesModule } from '@universis/ngx-tables';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { OpenBadgesRoutingModule } from './admin-routing.module';
import { BadgeImagesHomeComponent } from './components/badge-images-home/badge-images-home.component';
import { BadgeImagesTableComponent } from './components/badge-images-table/badge-images-table.component';
import { BadgeImagesRootComponent } from './components/badge-images-root/badge-images-root.component';
import { GenericTableConfigurationResolver, GenericTableSearchResolver, StudentDefaultTableConfigurationResolver } from './components/generic/generic-table-config.resolver';
import { BadgeClassesHomeComponent } from './components/badge-classes-home/badge-classes-home.component';
import { BadgeClassesTableComponent } from './components/badge-classes-table/badge-classes-table.component';
import { BadgeClassesRootComponent } from './components/badge-classes-root/badge-classes-root.component';
import { BadgeClassesDashboardComponent } from './components/badge-classes-dashboard/badge-classes-dashboard.component';
import { BadgeClassesOverviewComponent } from './components/badge-classes-dashboard/badge-classes-dashboard-overview/badge-classes-overview.component';
import { BadgeProfilesHomeComponent } from './components/badge-profiles-home/badge-profiles-home.component';
import { BadgeProfilesTableComponent } from './components/badge-profiles-table/badge-profiles-table.component';
import { BadgeProfilesRootComponent } from './components/badge-profiles-root/badge-profiles-root.component';
import { BadgeClassesDashboardStudentsComponent } from './components/badge-classes-dashboard/badge-classes-dashborad-students/badge-classes-dashboard-students.component';
import { BadgeClassesAddStudentComponent } from './components/badge-classes-dashboard/badge-classes-dashborad-students/badge-classes-add-student.component';
import { RouterModalModule } from '@universis/common/routing';
import { BadgeAssertionsViewComponent } from './components/badge-assertions-view/badge-assertions-view.component';
import { DataModelRule, RuleConfiguration, RuleService, RulesModule } from '@universis/ngx-rules';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    RouterModalModule,
    FormsModule,
    OpenBadgesRoutingModule,
    AdvancedFormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    NgArrayPipesModule,
    NgxDropzoneModule,
    TablesModule,
    NgxDropzoneModule,
    NgxExtendedPdfViewerModule,
    RulesModule.forRoot(<RuleConfiguration>{
      types: [<DataModelRule>{
        entitySet: 'EducationalOccupationalCredentials',
        navigationProperty: 'Rules',
        children: [
          // TODO fill list
          'StudentRule',
          'CourseRule',
          'CourseTypeRule',
        ]
      }]
    })
  ],
  declarations: [
    BadgeClassesHomeComponent,
    BadgeClassesTableComponent,
    BadgeClassesRootComponent,
    BadgeClassesDashboardComponent,
    BadgeClassesOverviewComponent,
    BadgeClassesDashboardStudentsComponent,
    BadgeClassesAddStudentComponent,
    BadgeImagesHomeComponent,
    BadgeImagesTableComponent,
    BadgeImagesRootComponent,
    BadgeProfilesHomeComponent,
    BadgeProfilesTableComponent,
    BadgeProfilesRootComponent,
    BadgeAssertionsViewComponent
  ],
  exports: [
  ],
  providers: [
    GenericTableConfigurationResolver,
    GenericTableSearchResolver,
    StudentDefaultTableConfigurationResolver,
    RuleService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OpenBadgesAdminModule {
}
