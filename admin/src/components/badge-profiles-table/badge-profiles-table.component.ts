import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AdvancedRowActionComponent, AdvancedTableComponent, AdvancedTableDataResult } from '@universis/ngx-tables';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivatedTableService } from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { Observable, Subscription } from 'rxjs';
import { ConfigurationService, ErrorService, LoadingService, ModalService, UserActivityService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { ClientDataQueryable } from '@themost/client';

@Component({
  selector: 'app-badge-profiles-table',
  templateUrl: './badge-profiles-table.component.html'
})
export class BadgeProfilesTableComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  public defaultLocale: string;
  private dataSubscription: Subscription;
  private selectedItems = [];
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _loadingService: LoadingService,
    private _modalService: ModalService,
    private _errorService: ErrorService,
    private _context: AngularDataContext,
    private _configurationService: ConfigurationService,
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = data.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
      this.defaultLocale = this._configurationService.settings
        && this._configurationService.settings.i18n
        && this._configurationService.settings.i18n.defaultLocale;
      this._userActivityService.setItem({
        category: this._translateService.instant('BadgeProfiles.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });

  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'name'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              user: item.user
            };
          });
        }
      }
    }
    return items;
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
