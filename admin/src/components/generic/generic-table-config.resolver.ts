import { Injectable } from '@angular/core';
import { TableConfiguration } from '@universis/ngx-tables';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, share } from 'rxjs/operators';

/**
 * Resolver require in the passed data the fields "model" and "config".
 */

@Injectable()
export class GenericTableConfigurationResolver implements Resolve<any> {

    constructor(private http: HttpClient) { }

    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<any> {
        const { model, config } = route.data;
        if (route.params.list == null) {
            return this.http.get<TableConfiguration>(`assets/tables/${model}/${config}-table.config.list.json`);
        }
        return this.http.get(`assets/tables/${model}/${config}-table.config.${route.params.list}.json`).pipe(
            map((result) => {
                return result as TableConfiguration;
            }),
            catchError((err) => {
                return this.http.get<TableConfiguration>(`assets/tables/${model}/${config}-table.config.list.json`);
            })
        );
    }
}

@Injectable()
export class GenericTableSearchResolver implements Resolve<any> {
    constructor(private http: HttpClient) { }

    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<any> {
        const { model, config } = route.data;
        if (route.params.list == null) {
            return this.http.get<TableConfiguration>(`assets/tables/${model}/${config}-table.search.list.json`);
        }
        return this.http.get(`assets/tables/${model}/${config}-table.search.${route.params.list}.json`).pipe(
            map((result) => {
                return result as TableConfiguration;
            }),
            catchError((err) => {
                return this.http.get<TableConfiguration>(`assets/tables/${model}/${config}-table.search.list.json`);
            })
        )
    }
}


// TODO rename or remove
@Injectable()
export class StudentDefaultTableConfigurationResolver implements Resolve<TableConfiguration> {

    public source$: Observable<TableConfiguration>;

    constructor(private http: HttpClient) {
        this.source$ = this.http.get<TableConfiguration>(`assets/tables/OpenBadgeClasses/students-table.config.list.json`).pipe(
                share()
        );
    }

    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<TableConfiguration> {
        return this.source$;
    }

    get(): Observable<TableConfiguration> {
        return this.source$;
    }
}