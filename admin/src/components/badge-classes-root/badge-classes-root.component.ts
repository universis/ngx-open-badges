import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TableConfiguration } from '@universis/ngx-tables';
import { cloneDeep } from 'lodash';
import { ReferrerRouteService, TemplatePipe } from '@universis/common';
import { Subscription } from 'rxjs';
import { GenericTableConfigurationResolver } from '../generic/generic-table-config.resolver';

@Component({
  selector: 'app-badge-classes-root',
  templateUrl: './badge-classes-root.component.html',
})
export class BadgeClassesRootComponent implements OnInit, OnDestroy {
  public model: any;
  public actions: any[];
  public allowedActions: any[];
  public edit: any;
  referrerSubscription: Subscription;
  paramSubscription: Subscription;
  referrerRoute: any = {
    queryParams: {},
    commands: []
  };

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _context: AngularDataContext,
    private _template: TemplatePipe,
    private _resolver: GenericTableConfigurationResolver,
    public referrer: ReferrerRouteService) {
  }
  ngOnDestroy(): void {
    if (this.referrerSubscription) {
      this.referrerSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((params: { id?: string }) => {
      this._context.model('OpenBadgeClasses')
        .where('_id').equal(params.id)
        .getItem().then((data) => {
          this.model = data;
          this._resolver.resolve(this._activatedRoute.snapshot, this._router.routerState.snapshot).subscribe((config: TableConfiguration) => {

            // get referrer
            this.referrerSubscription = this.referrer.routeParams$.subscribe(result => {
              const pathConfiguration = config as { paths?: { name: string, alternateName: string }[] };
              let redirectCommands = [
                '/', 'badges', 'classes'
              ]
              if (pathConfiguration.paths && pathConfiguration.paths.length) {
                const addCommands = pathConfiguration.paths[0].alternateName.split('/');
                redirectCommands = [
                  '/', 'badges', 'classes'
                ].concat(addCommands);
              }
              this.referrerRoute.commands = redirectCommands;
            });

            if (config.columns && this.model) {
              // get actions from config file
              this.actions = config.columns.filter((x: any) => {
                return x.actions;
              })
                // map actions
                .map((x: any) => x.actions)
                // get list items
                .reduce((a: any, b: any) => b, 0);

              // filter actions with student permissions
              this.allowedActions = this.actions.filter(x => {
                if (x.role) {
                  if (x.role === 'action') {
                    return x;
                  }
                }
              });

              this.edit = this.actions.find(x => {
                if (x.role === 'edit') {
                  x.href = this._template.transform(x.href, this.model);
                  return x;
                }
              });
              this.actions = this.allowedActions;
              this.actions.forEach(action => {
                action.href = this._template.transform(action.href, this.model);
              });
            }
          });
        });
    });

  }
}
