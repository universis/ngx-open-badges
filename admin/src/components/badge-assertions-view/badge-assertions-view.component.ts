import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer, SafeHtml, SafeResourceUrl } from '@angular/platform-browser';


@Component({
  selector: 'app-badge-assertions-view',
  templateUrl: './badge-assertions-view.component.html',
  styleUrls: ['badge-assertions-view.component.scss']
})
export class BadgeAssertionsViewComponent implements OnInit, OnDestroy {

  public model: any;
  public fields: any[];

  public badgeData: string;
  public badgeImageFilename: string
  public badgeImageUrl: SafeResourceUrl

  public isLoading = true;
  private subscription: Subscription;

  constructor(private http: HttpClient,
    private sanitizer: DomSanitizer,
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.isLoading = true;
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('OpenBadgeAssertions')
        .where('_id').equal(params.id)
        .expand('recipient,student($expand=person),badge($expand=criteria,tags,issuer)')
        .getItem();

      const fullName = this.model.student ?
        `${this.model.student.person.familyName} ${this.model.student.person.givenName}` : ''
      this.fields = [
        { name: "OpenBadges.BadgeAssertions.BadgeClass", value: this.model.badge.name },
        { name: "OpenBadges.BadgeAssertions.Recipient", value: fullName },
        { name: "OpenBadges.BadgeAssertions.RecipientEmail", value: this.model.recipient.identity },
        { name: "OpenBadges.BadgeAssertions.IssuedOn", value: this.model.issuedOn },
      ]

      this.badgeData = await this.http.get(this.model.id, { responseType: 'text' }).toPromise();
      
      const imageType = "svg"
      this.badgeImageFilename = `badge.${this.model._id}.${imageType}`;
      // TODO handle png
      const badgeImage = await this.http.get(this.model.id + "/image", { responseType: 'text' }).toPromise();
      this.badgeImageUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
        "data:image/svg+xml;charset=utf-8," + encodeURIComponent(badgeImage));

      this.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
