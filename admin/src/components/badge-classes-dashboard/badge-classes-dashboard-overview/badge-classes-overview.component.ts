import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-badge-classes-overview',
  templateUrl: './badge-classes-overview.component.html'
})
export class BadgeClassesOverviewComponent implements OnInit, OnDestroy {

  public model: any;
  public fields: any[];
  public isLoading = true;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.isLoading = true;
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('OpenBadgeClasses')
        .where('_id').equal(params.id)
        .expand('tags', 'criteria', 'image', 'issuer', 'educationalCredentials')
        .getItem();
      this.fields = [
        { name: "OpenBadges.BadgeClasses.Name", value: this.model.name },
        { name: "OpenBadges.BadgeClasses.Image", value: this.model.image.name },
        { name: "OpenBadges.BadgeClasses.Issuer", value: this.model.issuer.name },
      ]
      this.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
