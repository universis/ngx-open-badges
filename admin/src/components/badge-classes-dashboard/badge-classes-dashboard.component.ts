import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-badge-classes-dashboard',
  templateUrl: './badge-classes-dashboard.component.html'
})
export class BadgeClassesDashboardComponent implements OnInit, OnDestroy {

  public model: any;
  public tabs: any[];
  public isLoading = true;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.isLoading = true;
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.tabs = this._activatedRoute.routeConfig.children.filter(route => typeof route.redirectTo === 'undefined');
      this.model = await this._context.model('OpenBadgeClasses')
        .where('_id').equal(params.id)
        .expand('tags', 'criteria', 'image', 'issuer')
        .getItem();
      this.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
