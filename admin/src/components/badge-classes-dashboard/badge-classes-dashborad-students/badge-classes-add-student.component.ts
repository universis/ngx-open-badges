import { Component, Directive, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';

import { AdvancedTableModalBaseComponent, AdvancedTableModalBaseTemplate } from '@universis/ngx-tables';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs';
import { ErrorService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { AdvancedFilterValueProvider } from '@universis/ngx-tables';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-badge-classes-add-student',
  template: AdvancedTableModalBaseTemplate
})
export class BadgeClassesAddStudentComponent extends AdvancedTableModalBaseComponent {

  @Input() badgeClass: any;

  constructor(_router: Router, private _activatedRoute: ActivatedRoute,
    protected _context: AngularDataContext,
    private _errorService: ErrorService,
    private _toastService: ToastService,
    private _translateService: TranslateService,
    protected advancedFilterValueProvider: AdvancedFilterValueProvider,
    protected datePipe: DatePipe) {
    super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
    // set default title
    this.modalTitle = 'OpenBadges.AddStudent';
  }

  hasInputs(): Array<string> {
    return ['badgeClass'];
  }

  ok(): Promise<any> {
    console.log(this.badgeClass)
    // get selected items
    const selected = this.advancedTable.selected;
    if (selected && selected.length > 0) {
      // try to add classes
      const date_iso = new Date().toISOString();
      let items = selected.map(student => {
        // TODO handle if email is missing
        var email = student.email || `student.${student.id}@example.com`;
        return {
          student: student,
          recipient: {
            identity: email,
          },
          badge: this.badgeClass,
          issuedOn: date_iso,
        };
      });
      return this._context.model('OpenBadgeAssertions')
        .save(items)
        .then(result => {
          // add toast message
          this._toastService.show(
            this._translateService.instant('OpenBadges.AddStudentsMessage.title'),
            this._translateService.instant((items.length === 1 ?
              'OpenBadges.AddStudentsMessage.one' : 'OpenBadges.AddStudentsMessage.many')
              , { value: items.length })
          );
          return this.close({
            fragment: 'reload',
            skipLocationChange: true
          });
        }).catch(err => {
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    }
    return this.close();
    return Promise.resolve(false);
  }
}
