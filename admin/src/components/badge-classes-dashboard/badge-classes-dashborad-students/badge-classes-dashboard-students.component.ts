import { Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
// import * as SCHOLARSHIPS_STUDENTS_LIST_CONFIG from './badge-classes-dashboard-students.config.json';
import { ActivatedTableService } from '@universis/ngx-tables';
import { Subscription, combineLatest } from 'rxjs';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-badge-classes-dashboard-students',
  templateUrl: './badge-classes-dashboard-students.component.html'
})
export class BadgeClassesDashboardStudentsComponent implements OnInit, OnDestroy {

  // public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>SCHOLARSHIPS_STUDENTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;
  badgeClassID: any;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _translateService: TranslateService
  ) { }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  async ngOnInit() {
    this.subscription = combineLatest(
      this._activatedRoute.params,
      this._activatedRoute.data
    ).subscribe((results: any) => {
      const [params, data] = results;
      this._activatedTable.activeTable = this.students;
      this.badgeClassID = params.id;
      this.students.query = this._context.model('OpenBadgeAssertions')
        .where('badge/_id').equal(params.id)
        .expand('recipient,badge')
        .prepare();
      this.students.config = data.tableConfiguration

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });
    });

  }

  remove() {
    if (this.students && this.students.selected && this.students.selected.length) {
      // get items to remove
      const items = this.students.selected.map(item => {
        return {
          _id: item._id,
        };
      });
      console.log(this.students.selected, items);
      return this._modalService.showWarningDialog(
        this._translateService.instant('OpenBadges.RemoveStudentTitle'),
        this._translateService.instant('OpenBadges.RemoveStudentMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('OpenBadgeAssertions')
              .remove(items)
              .then(() => {
                this._toastService.show(
                  this._translateService.instant('OpenBadges.RemoveStudentsMessage.title'),
                  this._translateService.instant((items.length === 1 ?
                    'OpenBadges.RemoveStudentsMessage.one' : 'OpenBadges.RemoveStudentsMessage.many')
                    , { value: items.length })
                );
                this.students.fetch(true);
              }).catch(err => {
                this._errorService.showError(err, {
                  continueLink: '.'
                });
              });
          }
        });
    }
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
