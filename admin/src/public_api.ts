
export * from './components/badge-assertions-view/badge-assertions-view.component';
export * from './components/badge-classes-dashboard/badge-classes-dashboard.component';
export * from './components/badge-classes-dashboard/badge-classes-dashborad-students/badge-classes-add-student.component';
export * from './components/badge-classes-dashboard/badge-classes-dashborad-students/badge-classes-dashboard-students.component';
export * from './components/badge-classes-home/badge-classes-home.component';
export * from './components/badge-classes-root/badge-classes-root.component';
export * from './components/badge-classes-table/badge-classes-table.component';
export * from './components/badge-images-home/badge-images-home.component';
export * from './components/badge-images-root/badge-images-root.component';
export * from './components/badge-images-table/badge-images-table.component';
export * from './components/badge-profiles-home/badge-profiles-home.component';
export * from './components/badge-profiles-root/badge-profiles-root.component';
export * from './components/badge-profiles-table/badge-profiles-table.component';
export * from './components/generic/generic-table-config.resolver';

export * from './admin.module';
export * from './admin-routing.module';
