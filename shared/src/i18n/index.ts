import { en } from './open-badges.en';
import { el } from './open-badges.el';

export const OPEN_BADGES_LOCALES = {
  en,
  el
};
