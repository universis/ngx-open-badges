/* tslint:disable max-line-length */
// tslint:disable: quotemark
export const el = {
  "OpenBadges": {
    "Badge": "Δεξιότητα",
    "Badges": "Δεξιότητες",
    "BadgeClasses": {
      "Title": "Τύποι Δεξιότητας",
      "TitleSingular": "Τύπος Δεξιότητας",
      "NewTitle": "Νέος Τύπος Δεξιότητας",
      "Description": "Μια συλλογή πληροφοριών σχετικά με το επίτευγμα που αναγνωρίζεται από το Open Badge. Μπορεί να δημιουργηθούν πολλοί ισχυρισμοί που αντιστοιχούν σε μία κατηγορία δεξιοτήτων.",
      "Name": "Όνομα",
      "DescriptionForm": "Περιγραφή",
      "Image": "Εικόνα",
      "Issuer": "Εκδότης",
      "Tags": "Ετικέτες",
      "Criteria": "Κριτήρια",
      "New": "Δημιουργία",
      "Edit": "Επεξεργασία",
      "Preview": "Προβολή",
      "All": "Όλοι",
      "NewSimple": "Νέο Απλό",
      "NewComposite": "Νέο Σύνθετο",
      "Export": "Εξαγωγή",
      "Overview": "Επισκόπηση",
      "Details": "Στοιχεία Τύπου Δεξιότητας",
      "Students": "Φοιτητές",
      "RulesDetails": "Στοιχεία Προϋποθέσεων",
      "EditRules": "Επεξεργασία Προϋποθέσεων"
    },
    "BadgeImages": {
      "Title": "Εικόνες Δεξιότητας",
      "TitleSingular": "Εικόνα Δεξιότητας",
      "NewTitle": "Νέα Εικόνα Δεξιότητας",
      "Name": "Όνομα",
      "DescriptionForm": "Περιγραφή",
      "Image": "Εικόνα",
      "ContentUrl": "URL Εικόνας",
      "Export": "Εξαγωγή",
      "All": "Όλες",
      "New": "Δημιουργία",
      "Edit": "Επεξεργασία"
    },
    "BadgeProfiles": {
      "Title": "Προφίλ Δεξιότητας",
      "TitleSingular": "Προφίλ Δεξιότητας",
      "NewTitle": "Νέο Προφίλ Δεξιότητας",
      "Name": "Όνομα",
      "DescriptionForm": "Περιγραφή",
      "URL": "URL",
      "Tel": "Τηλ.",
      "Email": "Email",
      "Export": "Εξαγωγή",
      "All": "Όλα",
      "New": "Δημιουργία",
      "Edit": "Επεξεργασία"
    },
    "Students": "Φοιτητές",
    "AddStudent": "Εισαγωγή φοιτητών",
    "RemoveStudent": "Διαγραφή Φοιτητή",
    "StudentPreview": "Προβολή",
    "BadgeAssertions": {
      "Details": "Στοιχεία Assertion Δεξιότητας",
      "Recipient": "Παραλήπτης",
      "RecipientEmail": "Email Παραλήπτη",
      "BadgeClass": "Τύπος Δεξιότητας",
      "Identity": "Ταυτοποίηση",
      "IssuedOn": "Εκδόθηκε",
      "DownloadAssertion": "Download Assertion",
      "ShareAssertion": "Share Assertion",
      "DifferentEmail": "Το τρέχον email του χρήστη δεν είναι το ίδιο με το email του παραλήπτη της δεξιότητας",
    }
  },
  "Sidebar": {
    "Badges": "Δεξιότητες",
    "BadgeClasses": "Τύποι",
    "BadgeImages": "Εικόνες",
    "BadgeProfiles": "Προφίλ",
    "BadgeAlignments": "Alignments", // TODO
  }
};
