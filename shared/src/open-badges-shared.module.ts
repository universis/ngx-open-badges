import { NgModule } from "@angular/core";
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { OPEN_BADGES_LOCALES } from './i18n/index';

@NgModule({
  declarations: [],
  imports: [
    TranslateModule
  ],
  exports: []
})

export class OpenBadgesSharedModule {
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.log('An error occurred while loading open badges shared module');
      console.error(err);
    });
  }

  static forRoot() {
    return {
      ngModule: OpenBadgesSharedModule
    };
  }

  async ngOnInit() {
    Object.keys(OPEN_BADGES_LOCALES).forEach(lang => {
      if (OPEN_BADGES_LOCALES.hasOwnProperty(lang)) {
        this._translateService.setTranslation(lang, OPEN_BADGES_LOCALES[lang], true);
      }
    });
  }
}
