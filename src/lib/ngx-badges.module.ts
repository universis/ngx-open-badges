import { NgModule } from '@angular/core';
import { NgxBadgesComponent } from './ngx-badges.component';

@NgModule({
  imports: [
  ],
  declarations: [NgxBadgesComponent],
  exports: [NgxBadgesComponent]
})
export class NgxBadgesModule { }
