import { TestBed } from '@angular/core/testing';

import { NgxBadgesService } from './ngx-badges.service';

describe('NgxBadgesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgxBadgesService = TestBed.get(NgxBadgesService);
    expect(service).toBeTruthy();
  });
});
